let s:current_dir = expand('%:p:h')

let s:kitchen_active = 0

if !exists('g:testkitchen_cmd')
  let g:testkitchen_cmd = 'kitchen'
endif

if !exists('g:testkitchen_runner')
  let g:testkitchen_runner = 'T'
endif

while s:current_dir !=? '/'

  if exists('g:testkitchen_debug')
    echom 'Debug: testing ' . s:current_dir . '/.kitchen.yml'
  endif

  if filereadable(s:current_dir . '/.kitchen.yml')
    if exists('g:testkitchen_debug')
      echom 'Debug: ' . s:current_dir . ' /.kitchen.yml found'
    endif

    let s:kitchen_active = 1
    break
  endif

  let s:current_dir = fnamemodify(s:current_dir, ':h')
endwhile

if s:kitchen_active && !exists('g:testkitchen_loaded')
  let g:testkitchen_loaded = 1

  if exists('g:testkitchen_debug')
    echom 'Debug: Kitchen activating'
  endif

  function! s:kitchenctl(cmd)
          if exists('b:testkitchen_container')
                  let l:cmd=':' . g:testkitchen_runner . ' ' . g:testkitchen_cmd . ' ' . a:cmd . ' ' . b:testkitchen_container
          elseif exists('g:testkitchen_container')
                  let l:cmd=':' . g:testkitchen_runner . ' ' . g:testkitchen_cmd . ' ' . a:cmd . ' ' . g:testkitchen_container
          else
                  let l:cmd=':' . g:testkitchen_runner . ' ' . g:testkitchen_cmd . ' ' . a:cmd
          endif

          execute l:cmd
  endfunction

  command! -nargs=1 Kitchenctl :call s:kitchenctl(<args>)

  nnoremap <leader>kl :T pscli kitchen list<CR>

  nnoremap <leader>kd :Kitchenctl 'destroy'<CR>
  nnoremap <leader>kc :Kitchenctl 'converge'<CR>
  nnoremap <leader>kv :Kitchenctl 'verify'<CR>
  nnoremap <leader>kt :Kitchenctl 'test'<CR>

endif
