TEST KITCHEN.vim
================

Just a few little helpers to integrate vim and test kitchen


REQUIRES
========

* neovim
* neoterm

Currently as is a quick hack (for now) this plugin has some deps.

I may work to abstract these away. Maybe. PR's welcome though!

COMMANDS:
=========

`Kitchenctl` takes a single arg which is passed to kitchen via the pscli. (this may be abstracted away also)

`:Kitchenctl "converge"`

To run the command against a specific instance you can set either `b:testkitchen_container` or `g:testkitchen_container`

`Kitchenctl` will then run the kitchen command against that instance.

Mappings:
=========

 `<leader>kl`
 	Kitchen list

`<leader>kd`
	Kitchen Destroy

`<leader>kc`
	Kitchen Converge

`<leader>kv`
	Kitchen Verify

`<leader>kt`
	Kitchen Test
